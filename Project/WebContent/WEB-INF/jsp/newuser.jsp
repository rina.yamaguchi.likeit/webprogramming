<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<title>ユーザ新規登録</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
</head>

<body>
	<h6>${userInfo.name}さん<a href="LogoutServlet" target="_blank">ログアウト</a></h6>

	<c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		 	${errMsg}
		</div>
	</c:if>

	<div class="container">
		<h1>ユーザ新規登録</h1>

			<form method="post" action="UserCreateServlet" class="form-horizontal">
			<div class="p">
				<p>ログインID<span style="margin-right: 5em;"></span><input type="text" name="loginId"></p>
				<p>パスワード<span style="margin-right: 5em;"></span><input type="password" name="password"></p>
				<p>パスワード（確認）<span style="margin-right: 1em;"></span><input type="password" name="confirmPassword"></p>
				<p>ユーザ名	<span style="margin-right: 6em;"></span><input type="text" name="name"></p>
				<p>生年月日<span style="margin-right: 6em;"></span><input type="text" name="birthDate"></p>
				<p><span style="margin-right: 6em;"></span><input type="submit" value="登録"></p>
				</div>
			</form>

			<a href="UserListServlet" target="_blank">戻る</a>

	</div>
</body>

</html>
