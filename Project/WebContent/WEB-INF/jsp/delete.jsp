<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>ユーザ消去確認</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
</head>

<body>
	<h6>${userInfo.name}さん<a href="LogoutServlet" target="_blank">ログアウト</a></h6>
	<div class="container">

		<h1>ユーザ消去確認</h1>

		<form method="post" action="UserDeleteServlet" class="form-horizontal">
		   <input type="hidden" name="id" value="${user.id}">
			<label for="loginId" >ログインID：${user.loginId}</label><br>
			<label for="loginId">を本当に消去してよろしいでしょうか。</label><br>
			<p><a href="UserListServlet">キャンセル</a><span style="margin-right: 6em;"></span><a href="UserListSeervlet"><input type="submit" value="OK"name="botton"></a></p>

		</form>

	</div>
</body>
</html>