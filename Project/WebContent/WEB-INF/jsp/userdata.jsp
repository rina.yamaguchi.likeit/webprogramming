<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<title>ユーザ情報詳細参照</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
</head>

<body>
	<h6>${userInfo.name}さん<a href="LogoutServlet" target="_blank">ログアウト</a></h6>
	<div class="container">

		<h1>ユーザ情報詳細参照</h1>

		<div class="row">

			<label for="loginId" class="col-sm-2 col-form-label"> ログインID</label>
			<div class="col-sm-10">${user.loginId}</div>


			<label for="loginId" class="col-sm-2 col-form-label">ユーザ名</label>
			<div class="col-sm-10">${user.name}</div>


			<label for="loginId" class="col-sm-2 col-form-label">生年月日</label>
			<div class="col-sm-10">${user.birthDate}</div>


			<label for="loginId" class="col-sm-2 col-form-label">登録日時</label>
			<div class="col-sm-10">${user.createDate}</div>


			<label for="loginId" class="col-sm-2 col-form-label">更新日時</label>
			<div class="col-sm-10">${user.updateDate}</div>
		</div>

		<div class="col-sm-10">
			<a href="UserListServlet" target="_blank">戻る</a>
		</div>

	</div>

</body>

</html>