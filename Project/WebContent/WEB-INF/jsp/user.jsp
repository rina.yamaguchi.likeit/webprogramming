<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>ユーザ一覧</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
</head>

<body>
	<h6>${userInfo.name}さん<a href="LogoutServlet" target="_blank">ログアウト</a>
	</h6>
	<div class="container">
		<h1>ユーザ一覧</h1>
		<a href="UserCreateServlet" target="_blank">新規登録</a>

		<form method="post" action="UserListServlet" class="form-horizontal">
			<div class="p">
				<p>
					ログインID<span style="margin-right: 1em;"></span><input type="text"
						name="loginId">
				</p>
				<p>
					ユーザ名<span style="margin-right: 2em;"></span><input type="text"
						name="name">
				</p>
				<p>
					生年月日<span style="margin-right: 2em;"></span><input type="date"
						name="fromBirthData">～<input type="date"
						name="toBirthData">
				</p>
			</div>

			<div>
				<input type="submit" value="検索">
			</div>
		</form>

		<table border="1">

			<tr>
				<th>ログインID</th>
				<th>ユーザ名</th>
				<th>生年月日</th>
				<th></th>
			<c:choose>
			<c:when test="${userInfo.loginId=='admin'}">
			<c:forEach var="user" items="${userList}">
			<tr>
					<td>${user.loginId}</td>
					<td>${user.name}</td>
					<td>${user.birthDate}</td>
			<td>
					<a class="btn btn-primary"href="UserDetailServlet?id=${user.id}">詳細</a>
				    <a class="btn btn-success" href="UserUpdateServlet?id=${user.id}">更新</a>
					<a class="btn btn-danger"href="UserDeleteServlet?id=${user.id}">削除</a> <!--
							<input type="submit" value="詳細"style="background-color: #8EB8FF;">
							<input type="submit" value="更新" style="background-color: #93FFAB;">
							<input type="submit" value="消去" style="background-color: #FF97C2;">
							-->
			</tr>
			</c:forEach>
		    </c:when>

			<c:otherwise>
			<c:forEach var="user" items="${userList}">
			<tr>
					<td>${user.loginId}</td>
					<td>${user.name}</td>
					<td>${user.birthDate}</td>
				    <td>
				    <a class="btn btn-primary"href="UserDetailServlet?id=${user.id}">詳細</a>
				    <c:if test="${userInfo.loginId==user.loginId}">
				    <a class="btn btn-success" href="UserUpdateServlet?id=${user.id}">更新</a>
					<a class="btn btn-danger"href="UserDeleteServlet?id=${user.id}">削除</a>
				    </c:if>
				    </td>
		    </tr>
			</c:forEach>
			</c:otherwise>
			</c:choose>
		</table>
	</div>
</body>
</html>
