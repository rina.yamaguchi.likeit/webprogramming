<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ログイン画面</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
</head>
<body>
	<div class="container">
		<h1>ログイン画面</h1>
		<c:if test="${errMsg != null}">
			<div class="alert alert-danger" role="alert">${errMsg}</div>
		</c:if>

		<form class="form-signin" action="LoginServlet" method="post">
			<div class="form-signi">
				<label for="loginId"> ログインID</label> <input type="text"
					name="loginId">
			</div>

			<div class="form-signin">
				<label for="password"> パスワード</label> <input type="password"
					name="password">
			</div>

			<div class="col-sm-12">
				<input type="submit" value="ログイン">
			</div>
		</form>

	</div>


</body>
</html>
