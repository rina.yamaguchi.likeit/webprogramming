package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import model.User;

/**
 * ユーザテーブル用のDao
 * @author takano
 *
 */
public class UserDao {

    /**
     * ログインIDとパスワードに紐づくユーザ情報を返す
     * @param loginId
     * @param password
     * @return
     */
    public User findByLoginInfo(String loginId, String password) {
        Connection conn = null;
        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            // SELECT文を準備
            String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

             // SELECTを実行し、結果表を取得
            String encode =encode(password);
            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setString(1, loginId);
            pStmt.setString(2, encode);
            ResultSet rs = pStmt.executeQuery();


            // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
            if (!rs.next()) {
                return null;
            }

            // 必要なデータのみインスタンスのフィールドに追加
            String loginIdData = rs.getString("login_id");
            String nameData = rs.getString("name");
            return new User(loginIdData, nameData);

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
    }


    /**
     * 全てのユーザ情報を取得する（一覧）
     * @return
     */
    public List<User> findAll() {
        Connection conn = null;
        List<User> userList = new ArrayList<User>();

        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            // SELECT文を準備
            // TODO: 実装!：管理者以外を取得するようSQLを変更する
            String sql = "SELECT * FROM user WHERE id <> 6";

             // SELECTを実行し、結果表を取得
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            // 結果表に格納されたレコードの内容を
            // Userインスタンスに設定し、ArrayListインスタンスに追加
            while (rs.next()) {
                int id = rs.getInt("id");
                String loginId = rs.getString("login_id");
                String name = rs.getString("name");
                Date birthDate = rs.getDate("birth_date");
                String password = rs.getString("password");
                String createDate = rs.getString("create_date");
                String updateDate = rs.getString("update_date");
                User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

                userList.add(user);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
        return userList;
    }

    /**
     * idを引数にして、idに紐づくユーザ情報を出力する
     * @param int id
     * @return User
     */
    public User findUserById(int id) {
        Connection conn = null;

        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            // SELECT文を準備
            String sql = "SELECT * FROM user WHERE id = ?";

             // SELECTを実行し、結果表を取得
            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setInt(1, id);
            ResultSet rs = pStmt.executeQuery();

            // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
            if (!rs.next()) {
                return null;
            }

            // 必要なデータのみインスタンスのフィールドに追加
            id = rs.getInt("id");
            String loginId = rs.getString("login_id");
            String name = rs.getString("name");
            Date birthDate = rs.getDate("birth_date");
            String password = rs.getString("password");
            String createDate = rs.getString("create_date");
            String updateDate = rs.getString("update_date");
            return new User(id, loginId, name, birthDate, password, createDate, updateDate);


        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
    }

    /**
     * idを引数にして、idに紐づくユーザ情報を削除する
     * @param int id
     */
    public void deleteUserById(int id) {
        Connection conn = null;
        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            // DELETE文を準備
            String sql = "DELETE FROM user WHERE id = ?";

             // DELETEを実行
            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setInt(1, id);
            pStmt.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * ユーザ情報を登録する(新規登録）
     * @param String loginId
     * @param String password
     * @param String name
     * @param String birthData
     */
    public void createNewUser(String loginId,String password,String name,String birthDate) {
        Connection conn = null;
        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            // INSERT文を準備
            String sql = "INSERT INTO user(login_id,name, birth_date, password, create_date, update_date)VALUES (?, ?, ?, ?, now(), now())";

             // INSERTを実行
            String encode =encode(password);
            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setString(1, loginId);
            pStmt.setString(2, name);
            pStmt.setDate(3, Date.valueOf(birthDate));
            pStmt.setString(4, encode);
            pStmt.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * ユーザ情報を更新する
     * @param int id
     * @param String loginId
     * @param String password
     * @param String name
     * @param String birthData
     */
    public void updateUser(int id,String password,String name,String birthDate) {
        Connection conn = null;
        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            // UPDATE文を準備
            String sql = "UPDATE user SET name = ?, birth_date = ?, password = ?, update_date = now() WHERE id = ?";

             // INSERTを実行
            String encode =encode(password);
            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setString(1, name);
            pStmt.setDate(2, Date.valueOf(birthDate));
            pStmt.setString(3, encode);
            pStmt.setInt(4, id);
            pStmt.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * 検索結果のユーザ情報を返す(検索）
     * @param String loginId
     * @param String name
     * @param String fromBirth
     * @param String toBirth
     * @return
     */
    public List<User> searchUser(String loginId, String name, String fromBirth, String toBirth) {
        Connection conn = null;
        List<User> userList = new ArrayList<User>();
        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            // SELECT文を準備
            String sql = "SELECT * FROM user WHERE ";

            // 生年月日で不等号を使うので個別でクエリ定義
            String loginIdQuery = "login_id = ? AND ";
            String nameQuery = "name = ? AND ";
            String fromBirthDateQuery = "birth_date >= ? AND ";
            String toBirthDateQuery = "birth_date <= ?";

            // List型で検索値を持つことで検索要素が増えても楽に足せる
            List<String> searchElements = new ArrayList<String>();
            List<Date> searchDateElements = new ArrayList<Date>();

            // 取得した値が空じゃない場合にクエリなどの追加を行う
            if (!loginId.isEmpty()) {
            	searchElements = createSearchList(searchElements,loginId);
            	sql = sql + loginIdQuery;
            }
            if (!name.isEmpty()) {
            	searchElements = createSearchList(searchElements,name);
            	sql = sql + nameQuery;
            }
            if (!fromBirth.isEmpty()) {
            	Date fromBirthDate = Date.valueOf(fromBirth);
            	searchDateElements = createDateSearchList(searchDateElements,fromBirthDate);
            	sql = sql + fromBirthDateQuery;
            }
            if (!toBirth.isEmpty()) {
            	Date toBirthDate = Date.valueOf(toBirth);
            	searchDateElements = createDateSearchList(searchDateElements,toBirthDate);
            	sql = sql + toBirthDateQuery;
            	//TODO 実装検索値が空だった場合
            }if(loginId.isEmpty()&& name.isEmpty()&& fromBirth.isEmpty()&& toBirth.isEmpty()) {
               	 sql ="SELECT*FROM user";
            }






            // sqlが"AND"でおわっている場合は削除
            sql = deleteConnectingWord(sql);

            // SELECTを実行し、結果表を取得
            PreparedStatement pStmt = conn.prepareStatement(sql);

            // Listに格納した値をsetしていく(forで回すことで検索要素の追加時も変更なしでいける。)
            int num = 0;
            for(int i = 0; i < searchElements.size(); ++i){
            	num = num + 1;
            	String search = searchElements.get(i);
            	pStmt.setString(num, search);
            }
            for(int i = 0; i < searchDateElements.size(); ++i){
            	num = num + 1;
            	Date search = searchDateElements.get(i);
            	pStmt.setDate(num, search);
            }

            ResultSet rs = pStmt.executeQuery();


            // 結果表に格納されたレコードの内容を
            // Userインスタンスに設定し、ArrayListインスタンスに追加
            while (rs.next()) {
                int id = rs.getInt("id");
                loginId = rs.getString("login_id");
                name = rs.getString("name");
                Date birthDate = rs.getDate("birth_date");
                String password = rs.getString("password");
                String createDate = rs.getString("create_date");
                String updateDate = rs.getString("update_date");
                User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

                userList.add(user);
            }

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
        return userList;
    }

    /**
     * 検索値をListに格納
     * @param List<String> searchElements
     * @param String searchText
     * @return List<String> searchElements
     */
    private List<String> createSearchList(List<String> searchElements, String searchText) {

    	Collections.addAll(searchElements, searchText);

    	return searchElements;
    }

    /**
     * 検索値をListに格納(Date型)
     * @param List<Date> searchElements
     * @param Date searchDate
     * @return List<Date> searchElements
     */
    private List<Date> createDateSearchList(List<Date> searchElements, Date searchDate) {

    	Collections.addAll(searchElements, searchDate);

    	return searchElements;
    }

    /**
     * 	sqlが"AND"でおわっている場合は削除
     * @param String sql
     * @return String sql
     */
    private String deleteConnectingWord(String sql) {
        if (sql.endsWith("AND ")) {
        	  int last = sql.lastIndexOf("AND");
        	  sql = sql.substring(0, last);
        }

        return sql;
    }

    /**
     * 暗号化
     * @return
     *
     */
    public String encode (String password ) {

    //ハッシュを生成したい元の文字列
    String source =password;
    //ハッシュ生成前にバイト配列に置き換える際のCharset
    Charset charset = StandardCharsets.UTF_8;
    //ハッシュアルゴリズム
    String algorithm = "MD5";

    //ハッシュ生成処理
    byte[] bytes;
    String result =null;
    try {
    	bytes= MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
    	result= DatatypeConverter.printHexBinary(bytes);

    }catch(NoSuchAlgorithmException e1) {
    	e1.printStackTrace();
    }
	return result;



    }
}



